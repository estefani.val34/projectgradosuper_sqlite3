ID   2A5D_HUMAN              Reviewed;         602 AA.
AC   Q14738; A8K3I9; B5BUA6; O00494; O00696; Q15171; Q5TC39;
DT   30-MAY-2000, integrated into UniProtKB/Swiss-Prot.
DT   01-NOV-1996, sequence version 1.
DT   26-FEB-2020, entry version 204.
DE   RecName: Full=Serine/threonine-protein phosphatase 2A 56 kDa regulatory subunit delta isoform;
DE   AltName: Full=PP2A B subunit isoform B'-delta;
DE   AltName: Full=PP2A B subunit isoform B56-delta;
DE   AltName: Full=PP2A B subunit isoform PR61-delta;
DE   AltName: Full=PP2A B subunit isoform R5-delta;
GN   Name=PPP2R5D;
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi; Mammalia;
OC   Eutheria; Euarchontoglires; Primates; Haplorrhini; Catarrhini; Hominidae;
OC   Homo.
OX   NCBI_TaxID=9606;
RN   [1]
RP   NUCLEOTIDE SEQUENCE [MRNA] (ISOFORM DELTA-1).
RC   TISSUE=Fetal brain;
RX   PubMed=8703017; DOI=10.1074/jbc.271.36.22081;
RA   McCright B., Rivers A.M., Audlin S., Virshup D.M.;
RT   "The B56 family of protein phosphatase 2A (PP2A) regulatory subunits
RT   encodes differentiation-induced phosphoproteins that target PP2A to both
RT   nucleus and cytoplasm.";
RL   J. Biol. Chem. 271:22081-22089(1996).
RN   [2]
RP   NUCLEOTIDE SEQUENCE [MRNA] (ISOFORMS DELTA-1 AND DELTA-3).
RC   TISSUE=Brain cortex;
RX   PubMed=9180267; DOI=10.1016/s0014-5793(97)00392-x;
RA   Tanabe O., Gomez G.A., Nishito Y., Usui H., Takeda M.;
RT   "Molecular heterogeneity of the cDNA encoding a 74-kDa regulatory subunit
RT   (B'' or delta) of human protein phosphatase 2A.";
RL   FEBS Lett. 408:52-56(1997).
RN   [3]
RP   NUCLEOTIDE SEQUENCE [MRNA] (ISOFORM DELTA-2), AND PROTEIN SEQUENCE OF
RP   501-508; 550-559; 573-580 AND 584-601 (DELTA-1).
RC   TISSUE=Bone marrow, and Brain cortex;
RX   PubMed=8566219; DOI=10.1016/0014-5793(95)01500-0;
RA   Tanabe O., Nagase T., Murakami T., Nozaki H., Usui H., Nishito Y.,
RA   Hayashi H., Kagamiyama H., Takeda M.;
RT   "Molecular cloning of a 74-kDa regulatory subunit (B'' or delta) of human
RT   protein phosphatase 2A.";
RL   FEBS Lett. 379:107-111(1996).
RN   [4]
RP   NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA] (ISOFORM DELTA-2).
RC   TISSUE=Heart;
RX   PubMed=14702039; DOI=10.1038/ng1285;
RA   Ota T., Suzuki Y., Nishikawa T., Otsuki T., Sugiyama T., Irie R.,
RA   Wakamatsu A., Hayashi K., Sato H., Nagai K., Kimura K., Makita H.,
RA   Sekine M., Obayashi M., Nishi T., Shibahara T., Tanaka T., Ishii S.,
RA   Yamamoto J., Saito K., Kawai Y., Isono Y., Nakamura Y., Nagahari K.,
RA   Murakami K., Yasuda T., Iwayanagi T., Wagatsuma M., Shiratori A., Sudo H.,
RA   Hosoiri T., Kaku Y., Kodaira H., Kondo H., Sugawara M., Takahashi M.,
RA   Kanda K., Yokoi T., Furuya T., Kikkawa E., Omura Y., Abe K., Kamihara K.,
RA   Katsuta N., Sato K., Tanikawa M., Yamazaki M., Ninomiya K., Ishibashi T.,
RA   Yamashita H., Murakawa K., Fujimori K., Tanai H., Kimata M., Watanabe M.,
RA   Hiraoka S., Chiba Y., Ishida S., Ono Y., Takiguchi S., Watanabe S.,
RA   Yosida M., Hotuta T., Kusano J., Kanehori K., Takahashi-Fujii A., Hara H.,
RA   Tanase T.-O., Nomura Y., Togiya S., Komai F., Hara R., Takeuchi K.,
RA   Arita M., Imose N., Musashino K., Yuuki H., Oshima A., Sasaki N.,
RA   Aotsuka S., Yoshikawa Y., Matsunawa H., Ichihara T., Shiohata N., Sano S.,
RA   Moriya S., Momiyama H., Satoh N., Takami S., Terashima Y., Suzuki O.,
RA   Nakagawa S., Senoh A., Mizoguchi H., Goto Y., Shimizu F., Wakebe H.,
RA   Hishigaki H., Watanabe T., Sugiyama A., Takemoto M., Kawakami B.,
RA   Yamazaki M., Watanabe K., Kumagai A., Itakura S., Fukuzumi Y., Fujimori Y.,
RA   Komiyama M., Tashiro H., Tanigami A., Fujiwara T., Ono T., Yamada K.,
RA   Fujii Y., Ozaki K., Hirao M., Ohmori Y., Kawabata A., Hikiji T.,
RA   Kobatake N., Inagaki H., Ikema Y., Okamoto S., Okitani R., Kawakami T.,
RA   Noguchi S., Itoh T., Shigeta K., Senba T., Matsumura K., Nakajima Y.,
RA   Mizuno T., Morinaga M., Sasaki M., Togashi T., Oyama M., Hata H.,
RA   Watanabe M., Komatsu T., Mizushima-Sugano J., Satoh T., Shirai Y.,
RA   Takahashi Y., Nakagawa K., Okumura K., Nagase T., Nomura N., Kikuchi H.,
RA   Masuho Y., Yamashita R., Nakai K., Yada T., Nakamura Y., Ohara O.,
RA   Isogai T., Sugano S.;
RT   "Complete sequencing and characterization of 21,243 full-length human
RT   cDNAs.";
RL   Nat. Genet. 36:40-45(2004).
RN   [5]
RP   NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA] (ISOFORM DELTA-1).
RX   PubMed=19054851; DOI=10.1038/nmeth.1273;
RA   Goshima N., Kawamura Y., Fukumoto A., Miura A., Honma R., Satoh R.,
RA   Wakamatsu A., Yamamoto J., Kimura K., Nishikawa T., Andoh T., Iida Y.,
RA   Ishikawa K., Ito E., Kagawa N., Kaminaga C., Kanehori K., Kawakami B.,
RA   Kenmochi K., Kimura R., Kobayashi M., Kuroita T., Kuwayama H., Maruyama Y.,
RA   Matsuo K., Minami K., Mitsubori M., Mori M., Morishita R., Murase A.,
RA   Nishikawa A., Nishikawa S., Okamoto T., Sakagami N., Sakamoto Y.,
RA   Sasaki Y., Seki T., Sono S., Sugiyama A., Sumiya T., Takayama T.,
RA   Takayama Y., Takeda H., Togashi T., Yahata K., Yamada H., Yanagisawa Y.,
RA   Endo Y., Imamoto F., Kisu Y., Tanaka S., Isogai T., Imai J., Watanabe S.,
RA   Nomura N.;
RT   "Human protein factory for converting the transcriptome into an in vitro-
RT   expressed proteome.";
RL   Nat. Methods 5:1011-1017(2008).
RN   [6]
RP   NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA].
RX   PubMed=14574404; DOI=10.1038/nature02055;
RA   Mungall A.J., Palmer S.A., Sims S.K., Edwards C.A., Ashurst J.L.,
RA   Wilming L., Jones M.C., Horton R., Hunt S.E., Scott C.E., Gilbert J.G.R.,
RA   Clamp M.E., Bethel G., Milne S., Ainscough R., Almeida J.P., Ambrose K.D.,
RA   Andrews T.D., Ashwell R.I.S., Babbage A.K., Bagguley C.L., Bailey J.,
RA   Banerjee R., Barker D.J., Barlow K.F., Bates K., Beare D.M., Beasley H.,
RA   Beasley O., Bird C.P., Blakey S.E., Bray-Allen S., Brook J., Brown A.J.,
RA   Brown J.Y., Burford D.C., Burrill W., Burton J., Carder C., Carter N.P.,
RA   Chapman J.C., Clark S.Y., Clark G., Clee C.M., Clegg S., Cobley V.,
RA   Collier R.E., Collins J.E., Colman L.K., Corby N.R., Coville G.J.,
RA   Culley K.M., Dhami P., Davies J., Dunn M., Earthrowl M.E., Ellington A.E.,
RA   Evans K.A., Faulkner L., Francis M.D., Frankish A., Frankland J.,
RA   French L., Garner P., Garnett J., Ghori M.J., Gilby L.M., Gillson C.J.,
RA   Glithero R.J., Grafham D.V., Grant M., Gribble S., Griffiths C.,
RA   Griffiths M.N.D., Hall R., Halls K.S., Hammond S., Harley J.L., Hart E.A.,
RA   Heath P.D., Heathcott R., Holmes S.J., Howden P.J., Howe K.L., Howell G.R.,
RA   Huckle E., Humphray S.J., Humphries M.D., Hunt A.R., Johnson C.M.,
RA   Joy A.A., Kay M., Keenan S.J., Kimberley A.M., King A., Laird G.K.,
RA   Langford C., Lawlor S., Leongamornlert D.A., Leversha M., Lloyd C.R.,
RA   Lloyd D.M., Loveland J.E., Lovell J., Martin S., Mashreghi-Mohammadi M.,
RA   Maslen G.L., Matthews L., McCann O.T., McLaren S.J., McLay K., McMurray A.,
RA   Moore M.J.F., Mullikin J.C., Niblett D., Nickerson T., Novik K.L.,
RA   Oliver K., Overton-Larty E.K., Parker A., Patel R., Pearce A.V., Peck A.I.,
RA   Phillimore B.J.C.T., Phillips S., Plumb R.W., Porter K.M., Ramsey Y.,
RA   Ranby S.A., Rice C.M., Ross M.T., Searle S.M., Sehra H.K., Sheridan E.,
RA   Skuce C.D., Smith S., Smith M., Spraggon L., Squares S.L., Steward C.A.,
RA   Sycamore N., Tamlyn-Hall G., Tester J., Theaker A.J., Thomas D.W.,
RA   Thorpe A., Tracey A., Tromans A., Tubby B., Wall M., Wallis J.M.,
RA   West A.P., White S.S., Whitehead S.L., Whittaker H., Wild A., Willey D.J.,
RA   Wilmer T.E., Wood J.M., Wray P.W., Wyatt J.C., Young L., Younger R.M.,
RA   Bentley D.R., Coulson A., Durbin R.M., Hubbard T., Sulston J.E., Dunham I.,
RA   Rogers J., Beck S.;
RT   "The DNA sequence and analysis of human chromosome 6.";
RL   Nature 425:805-811(2003).
RN   [7]
RP   NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA].
RA   Mural R.J., Istrail S., Sutton G.G., Florea L., Halpern A.L., Mobarry C.M.,
RA   Lippert R., Walenz B., Shatkay H., Dew I., Miller J.R., Flanigan M.J.,
RA   Edwards N.J., Bolanos R., Fasulo D., Halldorsson B.V., Hannenhalli S.,
RA   Turner R., Yooseph S., Lu F., Nusskern D.R., Shue B.C., Zheng X.H.,
RA   Zhong F., Delcher A.L., Huson D.H., Kravitz S.A., Mouchard L., Reinert K.,
RA   Remington K.A., Clark A.G., Waterman M.S., Eichler E.E., Adams M.D.,
RA   Hunkapiller M.W., Myers E.W., Venter J.C.;
RL   Submitted (JUL-2005) to the EMBL/GenBank/DDBJ databases.
RN   [8]
RP   NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA] (ISOFORMS DELTA-1 AND DELTA-2).
RC   TISSUE=Colon, Eye, and Kidney;
RX   PubMed=15489334; DOI=10.1101/gr.2596504;
RG   The MGC Project Team;
RT   "The status, quality, and expansion of the NIH full-length cDNA project:
RT   the Mammalian Gene Collection (MGC).";
RL   Genome Res. 14:2121-2127(2004).
RN   [9]
RP   INTERACTION WITH SGO1.
RX   PubMed=16541025; DOI=10.1038/nature04663;
RA   Kitajima T.S., Sakuno T., Ishiguro K., Iemura S., Natsume T.,
RA   Kawashima S.A., Watanabe Y.;
RT   "Shugoshin collaborates with protein phosphatase 2A to protect cohesin.";
RL   Nature 441:46-52(2006).
RN   [10]
RP   PHOSPHORYLATION [LARGE SCALE ANALYSIS] AT SER-598, AND IDENTIFICATION BY
RP   MASS SPECTROMETRY [LARGE SCALE ANALYSIS].
RC   TISSUE=Embryonic kidney;
RX   PubMed=17525332; DOI=10.1126/science.1140321;
RA   Matsuoka S., Ballif B.A., Smogorzewska A., McDonald E.R. III, Hurov K.E.,
RA   Luo J., Bakalarski C.E., Zhao Z., Solimini N., Lerenthal Y., Shiloh Y.,
RA   Gygi S.P., Elledge S.J.;
RT   "ATM and ATR substrate analysis reveals extensive protein networks
RT   responsive to DNA damage.";
RL   Science 316:1160-1166(2007).
RN   [11]
RP   PHOSPHORYLATION [LARGE SCALE ANALYSIS] AT SER-573, AND IDENTIFICATION BY
RP   MASS SPECTROMETRY [LARGE SCALE ANALYSIS].
RC   TISSUE=Cervix carcinoma;
RX   PubMed=18669648; DOI=10.1073/pnas.0805139105;
RA   Dephoure N., Zhou C., Villen J., Beausoleil S.A., Bakalarski C.E.,
RA   Elledge S.J., Gygi S.P.;
RT   "A quantitative atlas of mitotic phosphorylation.";
RL   Proc. Natl. Acad. Sci. U.S.A. 105:10762-10767(2008).
RN   [12]
RP   IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS].
RX   PubMed=21269460; DOI=10.1186/1752-0509-5-17;
RA   Burkard T.R., Planyavsky M., Kaupe I., Breitwieser F.P., Buerckstuemmer T.,
RA   Bennett K.L., Superti-Furga G., Colinge J.;
RT   "Initial characterization of the human central proteome.";
RL   BMC Syst. Biol. 5:17-17(2011).
RN   [13]
RP   PHOSPHORYLATION [LARGE SCALE ANALYSIS] AT SER-573, AND IDENTIFICATION BY
RP   MASS SPECTROMETRY [LARGE SCALE ANALYSIS].
RX   PubMed=21406692; DOI=10.1126/scisignal.2001570;
RA   Rigbolt K.T., Prokhorova T.A., Akimov V., Henningsen J., Johansen P.T.,
RA   Kratchmarova I., Kassem M., Mann M., Olsen J.V., Blagoev B.;
RT   "System-wide temporal characterization of the proteome and phosphoproteome
RT   of human embryonic stem cell differentiation.";
RL   Sci. Signal. 4:RS3-RS3(2011).
RN   [14]
RP   INTERACTION WITH ADCY8.
RX   PubMed=22976297; DOI=10.1242/jcs.111427;
RA   Willoughby D., Halls M.L., Everett K.L., Ciruela A., Skroblin P.,
RA   Klussmann E., Cooper D.M.;
RT   "A key phosphorylation site in AC8 mediates regulation of Ca(2+)-dependent
RT   cAMP dynamics by an AC8-AKAP79-PKA signalling complex.";
RL   J. Cell Sci. 125:5850-5859(2012).
RN   [15]
RP   PHOSPHORYLATION [LARGE SCALE ANALYSIS] AT THR-63; SER-88; SER-89; SER-90
RP   AND SER-573, AND IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE
RP   ANALYSIS].
RC   TISSUE=Cervix carcinoma, and Erythroleukemia;
RX   PubMed=23186163; DOI=10.1021/pr300630k;
RA   Zhou H., Di Palma S., Preisinger C., Peng M., Polat A.N., Heck A.J.,
RA   Mohammed S.;
RT   "Toward a comprehensive characterization of a human cancer cell
RT   phosphoproteome.";
RL   J. Proteome Res. 12:260-271(2013).
RN   [16]
RP   INVOLVEMENT IN MRD35, AND VARIANTS MRD35 LYS-198 AND ARG-201.
RX   PubMed=25533962; DOI=10.1038/nature14135;
RG   Deciphering Developmental Disorders Study;
RT   "Large-scale discovery of novel genetic causes of developmental
RT   disorders.";
RL   Nature 519:223-228(2015).
RN   [17]
RP   VARIANT SER-53.
RX   PubMed=23033978; DOI=10.1056/nejmoa1206524;
RA   de Ligt J., Willemsen M.H., van Bon B.W., Kleefstra T., Yntema H.G.,
RA   Kroes T., Vulto-van Silfhout A.T., Koolen D.A., de Vries P., Gilissen C.,
RA   del Rosario M., Hoischen A., Scheffer H., de Vries B.B., Brunner H.G.,
RA   Veltman J.A., Vissers L.E.;
RT   "Diagnostic exome sequencing in persons with severe intellectual
RT   disability.";
RL   N. Engl. J. Med. 367:1921-1929(2012).
RN   [18]
RP   VARIANTS MRD35 LYS-198; LYS-200; ARG-201 AND ARG-207, VARIANT SER-53,
RP   CHARACTERIZATION OF VARIANTS MRD35 LYS-198; LYS-200; ARG-201 AND ARG-207,
RP   AND CHARACTERIZATION OF VARIANT SER-53.
RX   PubMed=26168268; DOI=10.1172/jci79860;
RA   Houge G., Haesen D., Vissers L.E., Mehta S., Parker M.J., Wright M.,
RA   Vogt J., McKee S., Tolmie J.L., Cordeiro N., Kleefstra T., Willemsen M.H.,
RA   Reijnders M.R., Berland S., Hayman E., Lahat E., Brilstra E.H.,
RA   van Gassen K.L., Zonneveld-Huijssoon E., de Bie C.I., Hoischen A.,
RA   Eichler E.E., Holdhus R., Steen V.M., Doeskeland S.O., Hurles M.E.,
RA   FitzPatrick D.R., Janssens V.;
RT   "B56delta-related protein phosphatase 2A dysfunction identified in patients
RT   with intellectual disability.";
RL   J. Clin. Invest. 125:3051-3062(2015).
CC   -!- FUNCTION: The B regulatory subunit might modulate substrate selectivity
CC       and catalytic activity, and also might direct the localization of the
CC       catalytic enzyme to a particular subcellular compartment.
CC   -!- SUBUNIT: PP2A consists of a common heterodimeric core enzyme, composed
CC       of a 36 kDa catalytic subunit (subunit C) and a 65 kDa constant
CC       regulatory subunit (PR65 or subunit A), that associates with a variety
CC       of regulatory subunits. Proteins that associate with the core dimer
CC       include three families of regulatory subunits B (the R2/B/PR55/B55,
CC       R3/B''/PR72/PR130/PR59 and R5/B'/B56 families), the 48 kDa variable
CC       regulatory subunit, viral proteins, and cell signaling molecules.
CC       Interacts with SGO1. Interacts with ADCY8 (PubMed:22976297).
CC       {ECO:0000269|PubMed:16541025, ECO:0000269|PubMed:22976297}.
CC   -!- INTERACTION:
CC       O08785:Clock (xeno); NbExp=2; IntAct=EBI-396563, EBI-79859;
CC       A1L4K1:FSD2; NbExp=3; IntAct=EBI-396563, EBI-5661036;
CC       Q13136:PPFIA1; NbExp=3; IntAct=EBI-396563, EBI-745426;
CC       P30153:PPP2R1A; NbExp=11; IntAct=EBI-396563, EBI-302388;
CC       P30154:PPP2R1B; NbExp=3; IntAct=EBI-396563, EBI-357094;
CC       Q8N6Y0:USHBP1; NbExp=3; IntAct=EBI-396563, EBI-739895;
CC   -!- SUBCELLULAR LOCATION: Cytoplasm. Nucleus. Note=Nuclear in interphase,
CC       nuclear during mitosis.
CC   -!- ALTERNATIVE PRODUCTS:
CC       Event=Alternative splicing; Named isoforms=3;
CC       Name=Delta-1;
CC         IsoId=Q14738-1; Sequence=Displayed;
CC       Name=Delta-2;
CC         IsoId=Q14738-2; Sequence=VSP_005111;
CC       Name=Delta-3;
CC         IsoId=Q14738-3; Sequence=VSP_005110;
CC   -!- TISSUE SPECIFICITY: Isoform Delta-2 is widely expressed. Isoform Delta-
CC       1 is highly expressed in brain.
CC   -!- INDUCTION: By retinoic acid; in neuroblastoma cell lines.
CC   -!- DISEASE: Mental retardation, autosomal dominant 35 (MRD35)
CC       [MIM:616355]: A form of mental retardation, a disorder characterized by
CC       significantly below average general intellectual functioning associated
CC       with impairments in adaptive behavior and manifested during the
CC       developmental period. {ECO:0000269|PubMed:25533962,
CC       ECO:0000269|PubMed:26168268}. Note=The disease is caused by mutations
CC       affecting the gene represented in this entry.
CC   -!- SIMILARITY: Belongs to the phosphatase 2A regulatory subunit B56
CC       family. {ECO:0000305}.
CC   ---------------------------------------------------------------------------
CC   Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms
CC   Distributed under the Creative Commons Attribution (CC BY 4.0) License
CC   ---------------------------------------------------------------------------
DR   EMBL; L76702; AAB69751.1; -; mRNA.
DR   EMBL; AB000634; BAA20381.1; -; mRNA.
DR   EMBL; AB000635; BAA20382.1; -; mRNA.
DR   EMBL; D78360; BAA11372.1; -; mRNA.
DR   EMBL; AK290604; BAF83293.1; -; mRNA.
DR   EMBL; AB451342; BAG70156.1; -; mRNA.
DR   EMBL; AB451357; BAG70171.1; -; mRNA.
DR   EMBL; AL136304; -; NOT_ANNOTATED_CDS; Genomic_DNA.
DR   EMBL; CH471081; EAX04133.1; -; Genomic_DNA.
DR   EMBL; BC010692; AAH10692.1; -; mRNA.
DR   EMBL; BC001095; AAH01095.1; -; mRNA.
DR   EMBL; BC001175; AAH01175.1; -; mRNA.
DR   CCDS; CCDS43464.1; -. [Q14738-3]
DR   CCDS; CCDS4878.1; -. [Q14738-1]
DR   CCDS; CCDS55002.1; -. [Q14738-2]
DR   PIR; S68686; S68686.
DR   RefSeq; NP_001257405.1; NM_001270476.1.
DR   RefSeq; NP_006236.1; NM_006245.3. [Q14738-1]
DR   RefSeq; NP_851307.1; NM_180976.2. [Q14738-2]
DR   RefSeq; NP_851308.1; NM_180977.2. [Q14738-3]
DR   SMR; Q14738; -.
DR   BioGrid; 111520; 89.
DR   DIP; DIP-29961N; -.
DR   ELM; Q14738; -.
DR   IntAct; Q14738; 55.
DR   MINT; Q14738; -.
DR   STRING; 9606.ENSP00000417963; -.
DR   iPTMnet; Q14738; -.
DR   PhosphoSitePlus; Q14738; -.
DR   BioMuta; PPP2R5D; -.
DR   DMDM; 7387495; -.
DR   EPD; Q14738; -.
DR   jPOST; Q14738; -.
DR   MassIVE; Q14738; -.
DR   MaxQB; Q14738; -.
DR   PaxDb; Q14738; -.
DR   PeptideAtlas; Q14738; -.
DR   PRIDE; Q14738; -.
DR   ProteomicsDB; 60149; -.
DR   ProteomicsDB; 60150; -. [Q14738-2]
DR   ProteomicsDB; 60151; -. [Q14738-3]
DR   DNASU; 5528; -.
DR   Ensembl; ENST00000394110; ENSP00000377669; ENSG00000112640. [Q14738-2]
DR   Ensembl; ENST00000461010; ENSP00000420674; ENSG00000112640. [Q14738-3]
DR   Ensembl; ENST00000485511; ENSP00000417963; ENSG00000112640. [Q14738-1]
DR   GeneID; 5528; -.
DR   KEGG; hsa:5528; -.
DR   UCSC; uc003oth.5; human. [Q14738-1]
DR   CTD; 5528; -.
DR   DisGeNET; 5528; -.
DR   GeneCards; PPP2R5D; -.
DR   HGNC; HGNC:9312; PPP2R5D.
DR   HPA; HPA029045; -.
DR   HPA; HPA029046; -.
DR   MalaCards; PPP2R5D; -.
DR   MIM; 601646; gene.
DR   MIM; 616355; phenotype.
DR   neXtProt; NX_Q14738; -.
DR   OpenTargets; ENSG00000112640; -.
DR   Orphanet; 457279; Intellectual disability-macrocephaly-hypotonia-behavioral abnormalities syndrome.
DR   PharmGKB; PA33676; -.
DR   eggNOG; KOG2085; Eukaryota.
DR   eggNOG; ENOG410XQJW; LUCA.
DR   GeneTree; ENSGT00950000182781; -.
DR   HOGENOM; CLU_012437_3_1_1; -.
DR   InParanoid; Q14738; -.
DR   KO; K11584; -.
DR   OMA; DECSHEY; -.
DR   OrthoDB; 890437at2759; -.
DR   PhylomeDB; Q14738; -.
DR   TreeFam; TF105556; -.
DR   Reactome; R-HSA-141444; Amplification of signal from unattached kinetochores via a MAD2 inhibitory signal.
DR   Reactome; R-HSA-163685; Integration of energy metabolism.
DR   Reactome; R-HSA-163767; PP2A-mediated dephosphorylation of key metabolic factors.
DR   Reactome; R-HSA-180024; DARPP-32 events.
DR   Reactome; R-HSA-195253; Degradation of beta-catenin by the destruction complex.
DR   Reactome; R-HSA-196299; Beta-catenin phosphorylation cascade.
DR   Reactome; R-HSA-198753; ERK/MAPK targets.
DR   Reactome; R-HSA-202670; ERKs are inactivated.
DR   Reactome; R-HSA-2467813; Separation of Sister Chromatids.
DR   Reactome; R-HSA-2500257; Resolution of Sister Chromatid Cohesion.
DR   Reactome; R-HSA-389513; CTLA4 inhibitory signaling.
DR   Reactome; R-HSA-432142; Platelet sensitization by LDL.
DR   Reactome; R-HSA-4641262; Disassembly of the destruction complex and recruitment of AXIN to the membrane.
DR   Reactome; R-HSA-5339716; Misspliced GSK3beta mutants stabilize beta-catenin.
DR   Reactome; R-HSA-5358747; S33 mutants of beta-catenin aren't phosphorylated.
DR   Reactome; R-HSA-5358749; S37 mutants of beta-catenin aren't phosphorylated.
DR   Reactome; R-HSA-5358751; S45 mutants of beta-catenin aren't phosphorylated.
DR   Reactome; R-HSA-5358752; T41 mutants of beta-catenin aren't phosphorylated.
DR   Reactome; R-HSA-5467337; APC truncation mutants have impaired AXIN binding.
DR   Reactome; R-HSA-5467340; AXIN missense mutants destabilize the destruction complex.
DR   Reactome; R-HSA-5467348; Truncations of AMER1 destabilize the destruction complex.
DR   Reactome; R-HSA-5663220; RHO GTPases Activate Formins.
DR   Reactome; R-HSA-5673000; RAF activation.
DR   Reactome; R-HSA-5675221; Negative regulation of MAPK pathway.
DR   Reactome; R-HSA-6811558; PI5P, PP2A and IER3 Regulate PI3K/AKT Signaling.
DR   Reactome; R-HSA-68877; Mitotic Prometaphase.
DR   Reactome; R-HSA-9634600; Regulation of glycolysis by fructose 2,6-bisphosphate metabolism.
DR   SignaLink; Q14738; -.
DR   SIGNOR; Q14738; -.
DR   ChiTaRS; PPP2R5D; human.
DR   GeneWiki; PPP2R5D; -.
DR   GenomeRNAi; 5528; -.
DR   Pharos; Q14738; Tbio.
DR   PRO; PR:Q14738; -.
DR   Proteomes; UP000005640; Chromosome 6.
DR   RNAct; Q14738; protein.
DR   Bgee; ENSG00000112640; Expressed in dorsal plus ventral thalamus and 226 other tissues.
DR   ExpressionAtlas; Q14738; baseline and differential.
DR   Genevisible; Q14738; HS.
DR   GO; GO:0005829; C:cytosol; IBA:GO_Central.
DR   GO; GO:0005654; C:nucleoplasm; TAS:Reactome.
DR   GO; GO:0005634; C:nucleus; IBA:GO_Central.
DR   GO; GO:0000159; C:protein phosphatase type 2A complex; IBA:GO_Central.
DR   GO; GO:0004721; F:phosphoprotein phosphatase activity; IDA:UniProtKB.
DR   GO; GO:0072542; F:protein phosphatase activator activity; IBA:GO_Central.
DR   GO; GO:0019888; F:protein phosphatase regulator activity; TAS:ProtInc.
DR   GO; GO:0010801; P:negative regulation of peptidyl-threonine phosphorylation; IEA:Ensembl.
DR   GO; GO:0007399; P:nervous system development; TAS:ProtInc.
DR   GO; GO:0035307; P:positive regulation of protein dephosphorylation; IEA:Ensembl.
DR   GO; GO:0006470; P:protein dephosphorylation; IDA:UniProtKB.
DR   GO; GO:0031952; P:regulation of protein autophosphorylation; IBA:GO_Central.
DR   GO; GO:0007165; P:signal transduction; IEA:InterPro.
DR   Gene3D; 1.25.10.10; -; 1.
DR   InterPro; IPR011989; ARM-like.
DR   InterPro; IPR016024; ARM-type_fold.
DR   InterPro; IPR002554; PP2A_B56.
DR   PANTHER; PTHR10257; PTHR10257; 1.
DR   Pfam; PF01603; B56; 1.
DR   PIRSF; PIRSF028043; PP2A_B56; 1.
DR   SUPFAM; SSF48371; SSF48371; 1.
PE   1: Evidence at protein level;
KW   Alternative splicing; Cytoplasm; Direct protein sequencing;
KW   Disease mutation; Mental retardation; Nucleus; Phosphoprotein;
KW   Polymorphism; Reference proteome; Repeat.
FT   CHAIN           1..602
FT                   /note="Serine/threonine-protein phosphatase 2A 56 kDa
FT                   regulatory subunit delta isoform"
FT                   /id="PRO_0000071452"
FT   REPEAT          37..38
FT                   /note="1"
FT   REPEAT          39..40
FT                   /note="2"
FT   REPEAT          41..42
FT                   /note="3"
FT   REPEAT          43..44
FT                   /note="4"
FT   REPEAT          45..46
FT                   /note="5"
FT   REPEAT          47..48
FT                   /note="6; approximate"
FT   REPEAT          49..50
FT                   /note="7; approximate"
FT   REPEAT          51..52
FT                   /note="8"
FT   REGION          37..52
FT                   /note="8 X 2 AA approximate tandem repeats of Q-P"
FT   MOTIF           523..530
FT                   /note="SH3-binding; class I"
FT                   /evidence="ECO:0000255"
FT   MOTIF           548..565
FT                   /note="Nuclear localization signal"
FT                   /evidence="ECO:0000255"
FT   MOD_RES         63
FT                   /note="Phosphothreonine"
FT                   /evidence="ECO:0000244|PubMed:23186163"
FT   MOD_RES         88
FT                   /note="Phosphoserine"
FT                   /evidence="ECO:0000244|PubMed:23186163"
FT   MOD_RES         89
FT                   /note="Phosphoserine"
FT                   /evidence="ECO:0000244|PubMed:23186163"
FT   MOD_RES         90
FT                   /note="Phosphoserine"
FT                   /evidence="ECO:0000244|PubMed:23186163"
FT   MOD_RES         573
FT                   /note="Phosphoserine"
FT                   /evidence="ECO:0000244|PubMed:18669648,
FT                   ECO:0000244|PubMed:21406692, ECO:0000244|PubMed:23186163"
FT   MOD_RES         598
FT                   /note="Phosphoserine"
FT                   /evidence="ECO:0000244|PubMed:17525332"
FT   VAR_SEQ         11..116
FT                   /note="Missing (in isoform Delta-3)"
FT                   /evidence="ECO:0000303|PubMed:9180267"
FT                   /id="VSP_005110"
FT   VAR_SEQ         85..116
FT                   /note="Missing (in isoform Delta-2)"
FT                   /evidence="ECO:0000303|PubMed:14702039,
FT                   ECO:0000303|PubMed:15489334, ECO:0000303|PubMed:8566219"
FT                   /id="VSP_005111"
FT   VARIANT         53
FT                   /note="P -> S (found in a patient with delayed psychomotor
FT                   development, no speech and cataracts; no effect on binding
FT                   to subunit PPP2CA; no effect on binding to subunit PPP2R1A;
FT                   dbSNP:rs757369209)"
FT                   /evidence="ECO:0000269|PubMed:23033978,
FT                   ECO:0000269|PubMed:26168268"
FT                   /id="VAR_069414"
FT   VARIANT         198
FT                   /note="E -> K (in MRD35; decreases binding to subunit
FT                   PPP2CA; decreases binding to subunit PPP2R1A;
FT                   dbSNP:rs863225082)"
FT                   /evidence="ECO:0000269|PubMed:25533962,
FT                   ECO:0000269|PubMed:26168268"
FT                   /id="VAR_073708"
FT   VARIANT         200
FT                   /note="E -> K (in MRD35; decreases binding to subunit
FT                   PPP2CA; decreases binding to subunit PPP2R1A;
FT                   dbSNP:rs863225079)"
FT                   /evidence="ECO:0000269|PubMed:26168268"
FT                   /id="VAR_074491"
FT   VARIANT         201
FT                   /note="P -> R (in MRD35; decreases binding to subunit
FT                   PPP2CA; decreases binding to subunit PPP2R1A;
FT                   dbSNP:rs876657383)"
FT                   /evidence="ECO:0000269|PubMed:25533962,
FT                   ECO:0000269|PubMed:26168268"
FT                   /id="VAR_073709"
FT   VARIANT         207
FT                   /note="W -> R (in MRD35; decreases binding to subunit
FT                   PPP2CA; decreases binding to subunit PPP2R1A;
FT                   dbSNP:rs869320691)"
FT                   /evidence="ECO:0000269|PubMed:26168268"
FT                   /id="VAR_074492"
SQ   SEQUENCE   602 AA;  69992 MW;  F15F71AF4E565387 CRC64;
     MPYKLKKEKE PPKVAKCTAK PSSSGKDGGG ENTEEAQPQP QPQPQPQAQS QPPSSNKRPS
     NSTPPPTQLS KIKYSGGPQI VKKERRQSSS RFNLSKNREL QKLPALKDSP TQEREELFIQ
     KLRQCCVLFD FVSDPLSDLK FKEVKRAGLN EMVEYITHSR DVVTEAIYPE AVTMFSVNLF
     RTLPPSSNPT GAEFDPEEDE PTLEAAWPHL QLVYEFFLRF LESPDFQPNI AKKYIDQKFV
     LALLDLFDSE DPRERDFLKT ILHRIYGKFL GLRAYIRRQI NHIFYRFIYE TEHHNGIAEL
     LEILGSIING FALPLKEEHK MFLIRVLLPL HKVKSLSVYH PQLAYCVVQF LEKESSLTEP
     VIVGLLKFWP KTHSPKEVMF LNELEEILDV IEPSEFSKVM EPLFRQLAKC VSSPHFQVAE
     RALYYWNNEY IMSLISDNAA RVLPIMFPAL YRNSKSHWNK TIHGLIYNAL KLFMEMNQKL
     FDDCTQQYKA EKQKGRFRMK EREEMWQKIE ELARLNPQYP MFRAPPPLPP VYSMETETPT
     AEDIQLLKRT VETEAVQMLK DIKKEKVLLR RKSELPQDVY TIKALEAHKR AEEFLTASQE
     AL
//
