ID   2A5A_HUMAN              Reviewed;         486 AA.
AC   Q15172; B2R6D2; B7Z7L2; D3DT99; Q2NL72; Q5VVB2; Q8TBI9;
DT   30-MAY-2000, integrated into UniProtKB/Swiss-Prot.
DT   01-NOV-1996, sequence version 1.
DT   26-FEB-2020, entry version 186.
DE   RecName: Full=Serine/threonine-protein phosphatase 2A 56 kDa regulatory subunit alpha isoform;
DE   AltName: Full=PP2A B subunit isoform B'-alpha;
DE   AltName: Full=PP2A B subunit isoform B56-alpha;
DE   AltName: Full=PP2A B subunit isoform PR61-alpha;
DE            Short=PR61alpha;
DE   AltName: Full=PP2A B subunit isoform R5-alpha;
GN   Name=PPP2R5A;
OS   Homo sapiens (Human).
OC   Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi; Mammalia;
OC   Eutheria; Euarchontoglires; Primates; Haplorrhini; Catarrhini; Hominidae;
OC   Homo.
OX   NCBI_TaxID=9606;
RN   [1]
RP   NUCLEOTIDE SEQUENCE [MRNA] (ISOFORM 1).
RC   TISSUE=Mammary cancer;
RX   PubMed=7592815; DOI=10.1074/jbc.270.44.26123;
RA   McCright B., Virshup D.M.;
RT   "Identification of a new family of protein phosphatase 2A regulatory
RT   subunits.";
RL   J. Biol. Chem. 270:26123-26128(1995).
RN   [2]
RP   NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA] (ISOFORMS 1 AND 2).
RC   TISSUE=Testis, and Tongue;
RX   PubMed=14702039; DOI=10.1038/ng1285;
RA   Ota T., Suzuki Y., Nishikawa T., Otsuki T., Sugiyama T., Irie R.,
RA   Wakamatsu A., Hayashi K., Sato H., Nagai K., Kimura K., Makita H.,
RA   Sekine M., Obayashi M., Nishi T., Shibahara T., Tanaka T., Ishii S.,
RA   Yamamoto J., Saito K., Kawai Y., Isono Y., Nakamura Y., Nagahari K.,
RA   Murakami K., Yasuda T., Iwayanagi T., Wagatsuma M., Shiratori A., Sudo H.,
RA   Hosoiri T., Kaku Y., Kodaira H., Kondo H., Sugawara M., Takahashi M.,
RA   Kanda K., Yokoi T., Furuya T., Kikkawa E., Omura Y., Abe K., Kamihara K.,
RA   Katsuta N., Sato K., Tanikawa M., Yamazaki M., Ninomiya K., Ishibashi T.,
RA   Yamashita H., Murakawa K., Fujimori K., Tanai H., Kimata M., Watanabe M.,
RA   Hiraoka S., Chiba Y., Ishida S., Ono Y., Takiguchi S., Watanabe S.,
RA   Yosida M., Hotuta T., Kusano J., Kanehori K., Takahashi-Fujii A., Hara H.,
RA   Tanase T.-O., Nomura Y., Togiya S., Komai F., Hara R., Takeuchi K.,
RA   Arita M., Imose N., Musashino K., Yuuki H., Oshima A., Sasaki N.,
RA   Aotsuka S., Yoshikawa Y., Matsunawa H., Ichihara T., Shiohata N., Sano S.,
RA   Moriya S., Momiyama H., Satoh N., Takami S., Terashima Y., Suzuki O.,
RA   Nakagawa S., Senoh A., Mizoguchi H., Goto Y., Shimizu F., Wakebe H.,
RA   Hishigaki H., Watanabe T., Sugiyama A., Takemoto M., Kawakami B.,
RA   Yamazaki M., Watanabe K., Kumagai A., Itakura S., Fukuzumi Y., Fujimori Y.,
RA   Komiyama M., Tashiro H., Tanigami A., Fujiwara T., Ono T., Yamada K.,
RA   Fujii Y., Ozaki K., Hirao M., Ohmori Y., Kawabata A., Hikiji T.,
RA   Kobatake N., Inagaki H., Ikema Y., Okamoto S., Okitani R., Kawakami T.,
RA   Noguchi S., Itoh T., Shigeta K., Senba T., Matsumura K., Nakajima Y.,
RA   Mizuno T., Morinaga M., Sasaki M., Togashi T., Oyama M., Hata H.,
RA   Watanabe M., Komatsu T., Mizushima-Sugano J., Satoh T., Shirai Y.,
RA   Takahashi Y., Nakagawa K., Okumura K., Nagase T., Nomura N., Kikuchi H.,
RA   Masuho Y., Yamashita R., Nakai K., Yada T., Nakamura Y., Ohara O.,
RA   Isogai T., Sugano S.;
RT   "Complete sequencing and characterization of 21,243 full-length human
RT   cDNAs.";
RL   Nat. Genet. 36:40-45(2004).
RN   [3]
RP   NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA].
RX   PubMed=16710414; DOI=10.1038/nature04727;
RA   Gregory S.G., Barlow K.F., McLay K.E., Kaul R., Swarbreck D., Dunham A.,
RA   Scott C.E., Howe K.L., Woodfine K., Spencer C.C.A., Jones M.C., Gillson C.,
RA   Searle S., Zhou Y., Kokocinski F., McDonald L., Evans R., Phillips K.,
RA   Atkinson A., Cooper R., Jones C., Hall R.E., Andrews T.D., Lloyd C.,
RA   Ainscough R., Almeida J.P., Ambrose K.D., Anderson F., Andrew R.W.,
RA   Ashwell R.I.S., Aubin K., Babbage A.K., Bagguley C.L., Bailey J.,
RA   Beasley H., Bethel G., Bird C.P., Bray-Allen S., Brown J.Y., Brown A.J.,
RA   Buckley D., Burton J., Bye J., Carder C., Chapman J.C., Clark S.Y.,
RA   Clarke G., Clee C., Cobley V., Collier R.E., Corby N., Coville G.J.,
RA   Davies J., Deadman R., Dunn M., Earthrowl M., Ellington A.G., Errington H.,
RA   Frankish A., Frankland J., French L., Garner P., Garnett J., Gay L.,
RA   Ghori M.R.J., Gibson R., Gilby L.M., Gillett W., Glithero R.J.,
RA   Grafham D.V., Griffiths C., Griffiths-Jones S., Grocock R., Hammond S.,
RA   Harrison E.S.I., Hart E., Haugen E., Heath P.D., Holmes S., Holt K.,
RA   Howden P.J., Hunt A.R., Hunt S.E., Hunter G., Isherwood J., James R.,
RA   Johnson C., Johnson D., Joy A., Kay M., Kershaw J.K., Kibukawa M.,
RA   Kimberley A.M., King A., Knights A.J., Lad H., Laird G., Lawlor S.,
RA   Leongamornlert D.A., Lloyd D.M., Loveland J., Lovell J., Lush M.J.,
RA   Lyne R., Martin S., Mashreghi-Mohammadi M., Matthews L., Matthews N.S.W.,
RA   McLaren S., Milne S., Mistry S., Moore M.J.F., Nickerson T., O'Dell C.N.,
RA   Oliver K., Palmeiri A., Palmer S.A., Parker A., Patel D., Pearce A.V.,
RA   Peck A.I., Pelan S., Phelps K., Phillimore B.J., Plumb R., Rajan J.,
RA   Raymond C., Rouse G., Saenphimmachak C., Sehra H.K., Sheridan E.,
RA   Shownkeen R., Sims S., Skuce C.D., Smith M., Steward C., Subramanian S.,
RA   Sycamore N., Tracey A., Tromans A., Van Helmond Z., Wall M., Wallis J.M.,
RA   White S., Whitehead S.L., Wilkinson J.E., Willey D.L., Williams H.,
RA   Wilming L., Wray P.W., Wu Z., Coulson A., Vaudin M., Sulston J.E.,
RA   Durbin R.M., Hubbard T., Wooster R., Dunham I., Carter N.P., McVean G.,
RA   Ross M.T., Harrow J., Olson M.V., Beck S., Rogers J., Bentley D.R.;
RT   "The DNA sequence and biological annotation of human chromosome 1.";
RL   Nature 441:315-321(2006).
RN   [4]
RP   NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA].
RA   Mural R.J., Istrail S., Sutton G.G., Florea L., Halpern A.L., Mobarry C.M.,
RA   Lippert R., Walenz B., Shatkay H., Dew I., Miller J.R., Flanigan M.J.,
RA   Edwards N.J., Bolanos R., Fasulo D., Halldorsson B.V., Hannenhalli S.,
RA   Turner R., Yooseph S., Lu F., Nusskern D.R., Shue B.C., Zheng X.H.,
RA   Zhong F., Delcher A.L., Huson D.H., Kravitz S.A., Mouchard L., Reinert K.,
RA   Remington K.A., Clark A.G., Waterman M.S., Eichler E.E., Adams M.D.,
RA   Hunkapiller M.W., Myers E.W., Venter J.C.;
RL   Submitted (SEP-2005) to the EMBL/GenBank/DDBJ databases.
RN   [5]
RP   NUCLEOTIDE SEQUENCE [LARGE SCALE MRNA] (ISOFORM 1).
RC   TISSUE=Hippocampus, and Testis;
RX   PubMed=15489334; DOI=10.1101/gr.2596504;
RG   The MGC Project Team;
RT   "The status, quality, and expansion of the NIH full-length cDNA project:
RT   the Mammalian Gene Collection (MGC).";
RL   Genome Res. 14:2121-2127(2004).
RN   [6]
RP   PROTEIN SEQUENCE OF 47-56; 129-132; 347-354; 448-462 AND 471-480.
RC   TISSUE=Brain;
RX   PubMed=8694763; DOI=10.1042/bj3170187;
RA   Zolnierowicz S., van Hoof C., Andjelkovic N., Cron P., Stevens I.,
RA   Merlevede W., Goris J., Hemmings B.A.;
RT   "The variable subunit associated with protein phosphatase 2A0 defines a
RT   novel multimember family of regulatory subunits.";
RL   Biochem. J. 317:187-194(1996).
RN   [7]
RP   PHOSPHORYLATION, AND SUBCELLULAR LOCATION.
RX   PubMed=8703017; DOI=10.1074/jbc.271.36.22081;
RA   McCright B., Rivers A.M., Audlin S., Virshup D.M.;
RT   "The B56 family of protein phosphatase 2A (PP2A) regulatory subunits
RT   encodes differentiation-induced phosphoproteins that target PP2A to both
RT   nucleus and cytoplasm.";
RL   J. Biol. Chem. 271:22081-22089(1996).
RN   [8]
RP   SUBCELLULAR LOCATION, AND INTERACTION WITH SGO1.
RX   PubMed=16541025; DOI=10.1038/nature04663;
RA   Kitajima T.S., Sakuno T., Ishiguro K., Iemura S., Natsume T.,
RA   Kawashima S.A., Watanabe Y.;
RT   "Shugoshin collaborates with protein phosphatase 2A to protect cohesin.";
RL   Nature 441:46-52(2006).
RN   [9]
RP   PHOSPHORYLATION [LARGE SCALE ANALYSIS] AT SER-41 AND SER-42, AND
RP   IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS].
RC   TISSUE=Cervix carcinoma;
RX   PubMed=18669648; DOI=10.1073/pnas.0805139105;
RA   Dephoure N., Zhou C., Villen J., Beausoleil S.A., Bakalarski C.E.,
RA   Elledge S.J., Gygi S.P.;
RT   "A quantitative atlas of mitotic phosphorylation.";
RL   Proc. Natl. Acad. Sci. U.S.A. 105:10762-10767(2008).
RN   [10]
RP   PHOSPHORYLATION [LARGE SCALE ANALYSIS] AT SER-41 AND SER-42, AND
RP   IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS].
RC   TISSUE=Cervix carcinoma;
RX   PubMed=20068231; DOI=10.1126/scisignal.2000475;
RA   Olsen J.V., Vermeulen M., Santamaria A., Kumar C., Miller M.L.,
RA   Jensen L.J., Gnad F., Cox J., Jensen T.S., Nigg E.A., Brunak S., Mann M.;
RT   "Quantitative phosphoproteomics reveals widespread full phosphorylation
RT   site occupancy during mitosis.";
RL   Sci. Signal. 3:RA3-RA3(2010).
RN   [11]
RP   IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS].
RX   PubMed=21269460; DOI=10.1186/1752-0509-5-17;
RA   Burkard T.R., Planyavsky M., Kaupe I., Breitwieser F.P., Buerckstuemmer T.,
RA   Bennett K.L., Superti-Furga G., Colinge J.;
RT   "Initial characterization of the human central proteome.";
RL   BMC Syst. Biol. 5:17-17(2011).
RN   [12]
RP   PHOSPHORYLATION [LARGE SCALE ANALYSIS] AT SER-41 AND SER-42, AND
RP   IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS].
RX   PubMed=21406692; DOI=10.1126/scisignal.2001570;
RA   Rigbolt K.T., Prokhorova T.A., Akimov V., Henningsen J., Johansen P.T.,
RA   Kratchmarova I., Kassem M., Mann M., Olsen J.V., Blagoev B.;
RT   "System-wide temporal characterization of the proteome and phosphoproteome
RT   of human embryonic stem cell differentiation.";
RL   Sci. Signal. 4:RS3-RS3(2011).
RN   [13]
RP   ACETYLATION [LARGE SCALE ANALYSIS] AT SER-2, CLEAVAGE OF INITIATOR
RP   METHIONINE [LARGE SCALE ANALYSIS], AND IDENTIFICATION BY MASS SPECTROMETRY
RP   [LARGE SCALE ANALYSIS].
RX   PubMed=22223895; DOI=10.1074/mcp.m111.015131;
RA   Bienvenut W.V., Sumpton D., Martinez A., Lilla S., Espagne C., Meinnel T.,
RA   Giglione C.;
RT   "Comparative large-scale characterisation of plant vs. mammal proteins
RT   reveals similar and idiosyncratic N-alpha acetylation features.";
RL   Mol. Cell. Proteomics 11:M111.015131-M111.015131(2012).
RN   [14]
RP   PHOSPHORYLATION [LARGE SCALE ANALYSIS] AT SER-41; SER-42 AND SER-49, AND
RP   IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS].
RC   TISSUE=Cervix carcinoma, and Erythroleukemia;
RX   PubMed=23186163; DOI=10.1021/pr300630k;
RA   Zhou H., Di Palma S., Preisinger C., Peng M., Polat A.N., Heck A.J.,
RA   Mohammed S.;
RT   "Toward a comprehensive characterization of a human cancer cell
RT   phosphoproteome.";
RL   J. Proteome Res. 12:260-271(2013).
RN   [15]
RP   IDENTIFICATION BY MASS SPECTROMETRY [LARGE SCALE ANALYSIS].
RC   TISSUE=Liver;
RX   PubMed=24275569; DOI=10.1016/j.jprot.2013.11.014;
RA   Bian Y., Song C., Cheng K., Dong M., Wang F., Huang J., Sun D., Wang L.,
RA   Ye M., Zou H.;
RT   "An enzyme assisted RP-RPLC approach for in-depth analysis of human liver
RT   phosphoproteome.";
RL   J. Proteomics 96:253-262(2014).
CC   -!- FUNCTION: The B regulatory subunit might modulate substrate selectivity
CC       and catalytic activity, and also might direct the localization of the
CC       catalytic enzyme to a particular subcellular compartment.
CC   -!- SUBUNIT: PP2A consists of a common heterodimeric core enzyme, composed
CC       of a 36 kDa catalytic subunit (subunit C) and a 65 kDa constant
CC       regulatory subunit (PR65 or subunit A), that associates with a variety
CC       of regulatory subunits. Proteins that associate with the core dimer
CC       include three families of regulatory subunits B (the R2/B/PR55/B55,
CC       R3/B''/PR72/PR130/PR59 and R5/B'/B56 families), the 48 kDa variable
CC       regulatory subunit, viral proteins, and cell signaling molecules.
CC       Interacts with SGO1. {ECO:0000269|PubMed:16541025}.
CC   -!- INTERACTION:
CC       O96017:CHEK2; NbExp=2; IntAct=EBI-641666, EBI-1180783;
CC       Q86YF9:DZIP1; NbExp=3; IntAct=EBI-641666, EBI-998108;
CC       Q9BVP2:GNL3; NbExp=3; IntAct=EBI-641666, EBI-641642;
CC       P00540:MOS; NbExp=3; IntAct=EBI-641666, EBI-1757866;
CC       P67775:PPP2CA; NbExp=5; IntAct=EBI-641666, EBI-712311;
CC       P30153:PPP2R1A; NbExp=6; IntAct=EBI-641666, EBI-302388;
CC       P30154:PPP2R1B; NbExp=2; IntAct=EBI-641666, EBI-357094;
CC       Q5FBB7:SGO1; NbExp=3; IntAct=EBI-641666, EBI-989069;
CC   -!- SUBCELLULAR LOCATION: Cytoplasm {ECO:0000269|PubMed:8703017}. Nucleus
CC       {ECO:0000269|PubMed:8703017}. Chromosome, centromere
CC       {ECO:0000269|PubMed:16541025}. Note=From mitotic prophase to metaphase,
CC       localizes at the inner centromere between a pair of sister
CC       kinetochores. Decreased expression at the onset of anaphase.
CC       {ECO:0000269|PubMed:16541025}.
CC   -!- ALTERNATIVE PRODUCTS:
CC       Event=Alternative splicing; Named isoforms=2;
CC       Name=1;
CC         IsoId=Q15172-1; Sequence=Displayed;
CC       Name=2;
CC         IsoId=Q15172-2; Sequence=VSP_042889;
CC   -!- TISSUE SPECIFICITY: Widely expressed with the highest expression in
CC       heart and skeletal muscle.
CC   -!- PTM: Phosphorylated on serine residues. {ECO:0000269|PubMed:8703017}.
CC   -!- SIMILARITY: Belongs to the phosphatase 2A regulatory subunit B56
CC       family. {ECO:0000305}.
CC   ---------------------------------------------------------------------------
CC   Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms
CC   Distributed under the Creative Commons Attribution (CC BY 4.0) License
CC   ---------------------------------------------------------------------------
DR   EMBL; L42373; AAC37601.1; -; mRNA.
DR   EMBL; AK302202; BAH13648.1; -; mRNA.
DR   EMBL; AK312530; BAG35429.1; -; mRNA.
DR   EMBL; AL451060; -; NOT_ANNOTATED_CDS; Genomic_DNA.
DR   EMBL; AL360091; -; NOT_ANNOTATED_CDS; Genomic_DNA.
DR   EMBL; CH471100; EAW93392.1; -; Genomic_DNA.
DR   EMBL; CH471100; EAW93393.1; -; Genomic_DNA.
DR   EMBL; BC022474; AAH22474.1; -; mRNA.
DR   EMBL; BC110883; AAI10884.1; -; mRNA.
DR   CCDS; CCDS1503.1; -. [Q15172-1]
DR   CCDS; CCDS55686.1; -. [Q15172-2]
DR   PIR; I55449; I55449.
DR   RefSeq; NP_001186685.1; NM_001199756.1. [Q15172-2]
DR   RefSeq; NP_006234.1; NM_006243.3. [Q15172-1]
DR   SMR; Q15172; -.
DR   BioGrid; 111517; 37.
DR   CORUM; Q15172; -.
DR   DIP; DIP-459N; -.
DR   ELM; Q15172; -.
DR   IntAct; Q15172; 39.
DR   MINT; Q15172; -.
DR   STRING; 9606.ENSP00000261461; -.
DR   BindingDB; Q15172; -.
DR   ChEMBL; CHEMBL4763; -.
DR   iPTMnet; Q15172; -.
DR   PhosphoSitePlus; Q15172; -.
DR   BioMuta; PPP2R5A; -.
DR   DMDM; 7387496; -.
DR   OGP; Q15172; -.
DR   EPD; Q15172; -.
DR   jPOST; Q15172; -.
DR   MassIVE; Q15172; -.
DR   MaxQB; Q15172; -.
DR   PaxDb; Q15172; -.
DR   PeptideAtlas; Q15172; -.
DR   PRIDE; Q15172; -.
DR   ProteomicsDB; 60481; -.
DR   ProteomicsDB; 60482; -. [Q15172-2]
DR   DNASU; 5525; -.
DR   Ensembl; ENST00000261461; ENSP00000261461; ENSG00000066027. [Q15172-1]
DR   Ensembl; ENST00000537030; ENSP00000442866; ENSG00000066027. [Q15172-2]
DR   GeneID; 5525; -.
DR   KEGG; hsa:5525; -.
DR   UCSC; uc001hjb.3; human. [Q15172-1]
DR   CTD; 5525; -.
DR   DisGeNET; 5525; -.
DR   GeneCards; PPP2R5A; -.
DR   HGNC; HGNC:9309; PPP2R5A.
DR   HPA; HPA059288; -.
DR   MIM; 601643; gene.
DR   neXtProt; NX_Q15172; -.
DR   OpenTargets; ENSG00000066027; -.
DR   PharmGKB; PA33672; -.
DR   eggNOG; KOG2085; Eukaryota.
DR   eggNOG; ENOG410XQJW; LUCA.
DR   GeneTree; ENSGT00950000182781; -.
DR   HOGENOM; CLU_012437_4_0_1; -.
DR   InParanoid; Q15172; -.
DR   KO; K11584; -.
DR   OMA; CCMLFDF; -.
DR   OrthoDB; 890437at2759; -.
DR   PhylomeDB; Q15172; -.
DR   TreeFam; TF105556; -.
DR   Reactome; R-HSA-141444; Amplification of signal from unattached kinetochores via a MAD2 inhibitory signal.
DR   Reactome; R-HSA-195253; Degradation of beta-catenin by the destruction complex.
DR   Reactome; R-HSA-196299; Beta-catenin phosphorylation cascade.
DR   Reactome; R-HSA-2467813; Separation of Sister Chromatids.
DR   Reactome; R-HSA-2500257; Resolution of Sister Chromatid Cohesion.
DR   Reactome; R-HSA-389513; CTLA4 inhibitory signaling.
DR   Reactome; R-HSA-432142; Platelet sensitization by LDL.
DR   Reactome; R-HSA-4641262; Disassembly of the destruction complex and recruitment of AXIN to the membrane.
DR   Reactome; R-HSA-5339716; Misspliced GSK3beta mutants stabilize beta-catenin.
DR   Reactome; R-HSA-5358747; S33 mutants of beta-catenin aren't phosphorylated.
DR   Reactome; R-HSA-5358749; S37 mutants of beta-catenin aren't phosphorylated.
DR   Reactome; R-HSA-5358751; S45 mutants of beta-catenin aren't phosphorylated.
DR   Reactome; R-HSA-5358752; T41 mutants of beta-catenin aren't phosphorylated.
DR   Reactome; R-HSA-5467337; APC truncation mutants have impaired AXIN binding.
DR   Reactome; R-HSA-5467340; AXIN missense mutants destabilize the destruction complex.
DR   Reactome; R-HSA-5467348; Truncations of AMER1 destabilize the destruction complex.
DR   Reactome; R-HSA-5663220; RHO GTPases Activate Formins.
DR   Reactome; R-HSA-5673000; RAF activation.
DR   Reactome; R-HSA-5675221; Negative regulation of MAPK pathway.
DR   Reactome; R-HSA-6811558; PI5P, PP2A and IER3 Regulate PI3K/AKT Signaling.
DR   Reactome; R-HSA-68877; Mitotic Prometaphase.
DR   SignaLink; Q15172; -.
DR   SIGNOR; Q15172; -.
DR   ChiTaRS; PPP2R5A; human.
DR   GeneWiki; PPP2R5A; -.
DR   GenomeRNAi; 5525; -.
DR   Pharos; Q15172; Tchem.
DR   PRO; PR:Q15172; -.
DR   Proteomes; UP000005640; Chromosome 1.
DR   RNAct; Q15172; protein.
DR   Bgee; ENSG00000066027; Expressed in lung and 229 other tissues.
DR   Genevisible; Q15172; HS.
DR   GO; GO:0005813; C:centrosome; IDA:UniProtKB.
DR   GO; GO:0000775; C:chromosome, centromeric region; IEA:UniProtKB-SubCell.
DR   GO; GO:0005737; C:cytoplasm; IDA:BHF-UCL.
DR   GO; GO:0005829; C:cytosol; IDA:HPA.
DR   GO; GO:0031430; C:M band; ISS:BHF-UCL.
DR   GO; GO:0016020; C:membrane; IDA:BHF-UCL.
DR   GO; GO:0005634; C:nucleus; IBA:GO_Central.
DR   GO; GO:0000159; C:protein phosphatase type 2A complex; IDA:UniProtKB.
DR   GO; GO:0030018; C:Z disc; IDA:BHF-UCL.
DR   GO; GO:0019900; F:kinase binding; IPI:BHF-UCL.
DR   GO; GO:0004721; F:phosphoprotein phosphatase activity; IDA:UniProtKB.
DR   GO; GO:0072542; F:protein phosphatase activator activity; IBA:GO_Central.
DR   GO; GO:0019888; F:protein phosphatase regulator activity; TAS:ProtInc.
DR   GO; GO:0090219; P:negative regulation of lipid kinase activity; IMP:BHF-UCL.
DR   GO; GO:1903077; P:negative regulation of protein localization to plasma membrane; IMP:BHF-UCL.
DR   GO; GO:0035307; P:positive regulation of protein dephosphorylation; IMP:BHF-UCL.
DR   GO; GO:0006470; P:protein dephosphorylation; IDA:UniProtKB.
DR   GO; GO:0031952; P:regulation of protein autophosphorylation; IBA:GO_Central.
DR   GO; GO:0007165; P:signal transduction; IEA:InterPro.
DR   Gene3D; 1.25.10.10; -; 1.
DR   InterPro; IPR011989; ARM-like.
DR   InterPro; IPR016024; ARM-type_fold.
DR   InterPro; IPR002554; PP2A_B56.
DR   PANTHER; PTHR10257; PTHR10257; 1.
DR   Pfam; PF01603; B56; 1.
DR   PIRSF; PIRSF028043; PP2A_B56; 1.
DR   SUPFAM; SSF48371; SSF48371; 1.
PE   1: Evidence at protein level;
KW   Acetylation; Alternative splicing; Centromere; Chromosome; Cytoplasm;
KW   Direct protein sequencing; Nucleus; Phosphoprotein; Reference proteome.
FT   INIT_MET        1
FT                   /note="Removed"
FT                   /evidence="ECO:0000244|PubMed:22223895"
FT   CHAIN           2..486
FT                   /note="Serine/threonine-protein phosphatase 2A 56 kDa
FT                   regulatory subunit alpha isoform"
FT                   /id="PRO_0000071448"
FT   COMPBIAS        2..5
FT                   /note="Poly-Ser"
FT   MOD_RES         2
FT                   /note="N-acetylserine"
FT                   /evidence="ECO:0000244|PubMed:22223895"
FT   MOD_RES         41
FT                   /note="Phosphoserine"
FT                   /evidence="ECO:0000244|PubMed:18669648,
FT                   ECO:0000244|PubMed:20068231, ECO:0000244|PubMed:21406692,
FT                   ECO:0000244|PubMed:23186163"
FT   MOD_RES         42
FT                   /note="Phosphoserine"
FT                   /evidence="ECO:0000244|PubMed:18669648,
FT                   ECO:0000244|PubMed:20068231, ECO:0000244|PubMed:21406692,
FT                   ECO:0000244|PubMed:23186163"
FT   MOD_RES         49
FT                   /note="Phosphoserine"
FT                   /evidence="ECO:0000244|PubMed:23186163"
FT   VAR_SEQ         1..61
FT                   /note="MSSSSPPAGAASAAISASEKVDGFTRKSVRKAQRQKRSQGSSQFRSQGSQAE
FT                   LHPLPQLKD -> MIMN (in isoform 2)"
FT                   /evidence="ECO:0000303|PubMed:14702039"
FT                   /id="VSP_042889"
FT   CONFLICT        52
FT                   /note="E -> F (in Ref. 6; AA sequence)"
FT                   /evidence="ECO:0000305"
FT   CONFLICT        54
FT                   /note="H -> S (in Ref. 6; AA sequence)"
FT                   /evidence="ECO:0000305"
FT   CONFLICT        176
FT                   /note="Q -> R (in Ref. 5; AAH22474)"
FT                   /evidence="ECO:0000305"
FT   CONFLICT        389
FT                   /note="D -> N (in Ref. 5; AAH22474)"
FT                   /evidence="ECO:0000305"
FT   CONFLICT        451
FT                   /note="R -> E (in Ref. 6; AA sequence)"
FT                   /evidence="ECO:0000305"
SQ   SEQUENCE   486 AA;  56194 MW;  D31407F7032A6D44 CRC64;
     MSSSSPPAGA ASAAISASEK VDGFTRKSVR KAQRQKRSQG SSQFRSQGSQ AELHPLPQLK
     DATSNEQQEL FCQKLQQCCI LFDFMDSVSD LKSKEIKRAT LNELVEYVST NRGVIVESAY
     SDIVKMISAN IFRTLPPSDN PDFDPEEDEP TLEASWPHIQ LVYEFFLRFL ESPDFQPSIA
     KRYIDQKFVQ QLLELFDSED PRERDFLKTV LHRIYGKFLG LRAFIRKQIN NIFLRFIYET
     EHFNGVAELL EILGSIINGF ALPLKAEHKQ FLMKVLIPMH TAKGLALFHA QLAYCVVQFL
     EKDTTLTEPV IRGLLKFWPK TCSQKEVMFL GEIEEILDVI EPTQFKKIEE PLFKQISKCV
     SSSHFQVAER ALYFWNNEYI LSLIEENIDK ILPIMFASLY KISKEHWNPT IVALVYNVLK
     TLMEMNGKLF DDLTSSYKAE RQREKKKELE REELWKKLEE LKLKKALEKQ NSAYNMHSIL
     SNTSAE
//
