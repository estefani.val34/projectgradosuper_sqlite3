from django.urls import path
from django.contrib import admin

from . import views
#https://www.hektorprofe.net/tutorial/django-sistema-registro-login-logout

urlpatterns = [
    path('', views.protein_list, name='protein_list'),
    path('protein/new/', views.protein_new, name='protein_new'),
   # path('users/<int:pk>/', views.users_detail, name='users_detail'),
    path('register', views.register),
    path('login', views.login),
    path('logout', views.logout),

    path('admin/', admin.site.urls),
]