from django import forms
from .models import Protein, Funtion
from django.forms import ModelForm

class ProteinForm(forms.ModelForm):
    functions = forms.ModelMultipleChoiceField(queryset=Funtion.objects.all(), 
    widget=forms.Select(), required=False)

    class Meta:
        model = Protein
        fields = ('__all__')
        exclude=['created_date']