from django.db import models
from django.utils import timezone

# una función puede estar en varios objetos de proteina y una
# proteina tiene varios objetos de funcion:

class Funtion(models.Model):
    function = models.TextField()
    catalytic_activity= models.TextField()
    activity_regulation	=models.TextField()
    subcellular_location=models.TextField()

    def __str__(self):
        return '%s ' % (self.function)

class Protein(models.Model):
    entry = models.CharField(max_length=50,primary_key=True,unique=True)
    entry_name = models.CharField(max_length=50, unique=True)
    protein_names = models.CharField(max_length=800)
    cross_reference_GeneID=models.CharField(max_length=20)
    gene_names = models.CharField(max_length=200)
    organism = models.CharField(max_length=80)
    taxonomy_lineage = models.CharField(max_length=300)
    sequence = models.TextField()
    length = models.PositiveIntegerField()
    seq_similarities = models.CharField(max_length = 300)
    functions = models.ManyToManyField(Funtion)
    created_date = models.DateTimeField(default=timezone.now)
    
    class Meta:
        ordering = ['entry']

    def __str__(self):
        return '%s %s' % (self.entry, self.entry_name)



